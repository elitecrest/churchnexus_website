﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChurchWebSite
{
    public partial class ChurchRegistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //protected void btnCancel_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("Index.aspx");
        //}

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ChurchDTO church = new ChurchDTO();
            church.Id = 0;
            church.Name = ChurchName.Text;
            church.AdminFirstName = AdminFirstName.Text;
            church.AdminLastName = AdminLastName.Text;
            church.City = ChurchCity.Text;
            church.ContactPersonName = church.AdminFirstName + " " + church.AdminLastName; //check
            church.PhoneNum = ContactNumber.Text; //check
            church.Address = ChurchAddress.Text;//check
            church.State = ChurchState.Text;
            church.Country = churchcountry.SelectedItem.Text;
            church.Email = Email.Text;//check
            church.ContactPersonEmail = Email.Text;
            church.Zip = ZipCode.Text;
            church.Enabled = true; //check
            church.Description = "";//check
            church.Geomap = ""; //check
            church.History = "";//check
            church.About = ""; //check
            //church.GalleryId = "";
            church.Banners = ""; //check
            church.Splashscreen = "";//check
            church.Theme = "";//check
            church.ChurchAPIKey = "";//check
            church.Latitude = "";//check
            church.Longitude = "";//check
            church.PlanId = 0;//check
            church.PlanTypeId = 1;
            church.OrderId = 0;
            church.IsKidsEnabled = 0;
            church.IsPayPal = false;
            church.Username = Email.Text;
            church.Password = Password.Text;//check
            church.ContactPersonPhoneNum = church.PhoneNum; //check
            church.BannerFormat = "";
            church.SplashScreenFormat = "";
            church.IsPushPay = true;
            church.GiveURL = (GiveURL.Text) != null ? GiveURL.Text : "";

            //church.LastModified = "";
            CustomResponse res = APICalls.Post("Churches/CreateChurch", church);
            results.Visible = true;
            results.Text = res.Message;
            if(res.Status == 0)
            {
                Response.Redirect("http://churchnexusadmintest.azurewebsites.net/");

            }
        }
    }
}