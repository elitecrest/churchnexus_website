﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChurchWebSite
{
    public partial class ChurchDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Boolean Enabled { get; set; }
        public String Geomap { get; set; }
        public String History { get; set; }
        public String About { get; set; }
        public Int16? GalleryId { get; set; }
        public DateTime? LastModified { get; set; }
        public String Banners { get; set; }
        public String Splashscreen { get; set; }
        public String Theme { get; set; }
        public String PhoneNum { get; set; }
        public String Email { get; set; }
        public String Address { get; set; }
        public String ChurchAPIKey { get; set; }
        public String Latitude { get; set; }
        public String Longitude { get; set; }
        public Int32 PlanId { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String ContactPersonName { get; set; }
        public String ContactPersonEmail { get; set; }
        public String ContactPersonPhoneNum { get; set; }
        public String BannerFormat { get; set; }
        public String SplashScreenFormat { get; set; }
        public Boolean IsPushPay { get; set; }
        public Boolean IsPayPal { get; set; }
        public String GiveURL { get; set; }
        public String State { get; set; }
        public String City { get; set; }
        public String Zip { get; set; }
        public String Country { get; set; }
        public Boolean IsContent { get; set; }
        public Boolean EnableStatus { get; set; }
       // public List<ChurchFeaturesDto> ChurchFeatures { get; set; }
        public string AdminFirstName { get; set; }
        public string AdminLastName { get; set; }
        public int PlanTypeId { get; set; }
        public int OrderId { get; set; }
        public int IsKidsEnabled { get; set; }
    }
    public class ChurchFeaturesDto
    {
        public int Id { get; set; }
        public string FeatureName { get; set; }
        public int ChurchId { get; set; }
    }
}