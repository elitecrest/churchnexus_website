﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net.Http.Formatting;
using System.Runtime.CompilerServices;
using System.Text;
using ChurchWebSite;

namespace ChurchWebSite
{
    public static class APICalls
    {
        static APICalls()
        {
            //initialize variables here
        }



        public static string baseurl = ConfigurationManager.AppSettings["BaseURl"].ToString();
        public static CustomResponse res = new CustomResponse();
        public static System.Web.Script.Serialization.JavaScriptSerializer javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();



        public static CustomResponse Get(string apiurl)
        {

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(baseurl);
                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync(apiurl).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<CustomResponse>().Result;
                }
                else
                {
                }
                return res;
            }
            catch
            {
                throw;
            }
        }

        public static CustomResponse Post(string apiurl, object data)
        {

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(baseurl);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.PostAsJsonAsync(apiurl, data).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<CustomResponse>().Result;
                }
                return res;
            }
            catch
            {
                throw;
            }
        }

        public static CustomResponse Put(string apiurl, object data)
        {
            javaScriptSerializer.MaxJsonLength = 2147483647;

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(baseurl);
                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string jsonstring = javaScriptSerializer.Serialize(data);
                StringContent content = new System.Net.Http.StringContent(jsonstring, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PutAsync(apiurl, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<CustomResponse>().Result;
                }
                else
                {
                }
                return res;

            }
            catch
            {
                throw;
            }

        }

        public static CustomResponse Delete(string apiurl)
        {

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(baseurl);
                HttpResponseMessage response = client.DeleteAsync(apiurl).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<CustomResponse>().Result;
                }
                return res;
            }
            catch
            {
                throw;
            }
        }

    }
}