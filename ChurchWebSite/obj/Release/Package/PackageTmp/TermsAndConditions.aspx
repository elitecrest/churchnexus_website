﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TermsAndConditions.aspx.cs" Inherits="ChurchWebSite.TermsAndConditions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Template Title -->
    <title>Church Nexus</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- Bootstrap 3.2.0 stylesheet -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome Icon stylesheet -->
    <link href="Content/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Owl Carousel stylesheet -->
    <link href="Content/owl.carousel.css" rel="stylesheet">
    <!-- Pretty Photo stylesheet -->
    <link href="Content/prettyPhoto.css" rel="stylesheet">
    <!-- Custom stylesheet -->
    <link href="Content/style.css" rel="stylesheet" type="text/css" />
    <link href="Content/white.css" rel="stylesheet" type="text/css" />

    <!-- Custom Responsive stylesheet -->
    <link href="Content/responsive.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="Scripts/jquery.min.js"></script>
    <script src="Scripts/jquery-1.10.2.js"></script>
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Scripts/jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
                    $("#RegisterUser").click(function () {
                        var data = {
                            type: '1',
                            price:'500'
                        }
                        window.location.href = "./ChurchRegistration.aspx?" + $.param(data);
                        return false;
                    });
                    $("#RegisterUser1").click(function () {
                        var data = {
                            type: '2',
                            price: '700'
                        }
                        window.location.href = "./ChurchRegistration.aspx?" + $.param(data);
                        return false;
                    });
                    $("#RegisterUser2").click(function () {
                        var data = {
                            type: '3',
                            price: '1000'
                        }
                        window.location.href = "./ChurchRegistration.aspx?" + $.param(data);
                        return false;
                    });
                    $("#RegisterUser3").click(function () {
                        var data = {
                            type: '4',
                            price: '1500'
                        }
                        window.location.href = "./ChurchRegistration.aspx?" + $.param(data);
                        return false;
                    });
        });
        </script>
</head>
<body>
    <form id="form1" runat="server">
        <!-- ====== Header Section ====== -->
       
        <!-- ====== End Header Section ====== -->
        <!-- ====== Menu Section ====== -->
        <section id="menu">
      <div class="navigation">
        <div id="main-nav" class="navbar navbar-default" role="navigation">
          <div class="container">

            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                 <div class="navbar-brand" > <img src="images/churchnexus1.png"/ alt=" " /></div>
              <%--<a class="navbar-brand" href="images/churchnexuslogo.png">Church Nexus</a>--%>
            </div> <!-- end .navbar-header -->

            <div class="navbar-collapse collapse">
             
            </div><!-- end .navbar-collapse -->

          </div> <!-- end .container -->
        </div> <!-- end #main-nav -->
      </div> <!-- end .navigation -->
    </section>
        <!-- ====== End Menu Section ====== -->
        <!-- ====== Features Section ====== -->
      
        <!-- ====== End Features Section ====== -->
        <!-- ====== Screenshots Section ====== -->
        <%--    <section id="screenshots">
      <div class="screenshots section-padding dark-bg">
        <div class="container">
          <div class="header">
            <h1><strong>W</strong>hy <strong>t</strong>his <strong>a</strong>pp?</h1>
            <p>Want more Bootstrap themes & templates? Subscribe to our mailing list to receive an update when new items arrive! Akshara is a library to buy Bootstrap themes and templates for your business need. Want more Bootstrap themes & templates ? Want more Bootstrap themes & templates?</p>
            <div class="underline"><i class="fa fa-image"></i></div>
          </div>

          <div class="owl-carousel owl-theme">
            <div class="item">
              <a href="images/app.jpg" data-rel="prettyPhoto"><img src="images/app.jpg" alt="item photo"></a>
            </div> <!-- end item -->
            <div class="item">
              <a href="images/app2.jpg" data-rel="prettyPhoto"><img src="images/app2.jpg" alt="item photo"></a>
            </div> <!-- end item -->
            <div class="item">
              <a href="images/app.jpg" data-rel="prettyPhoto"><img src="images/app.jpg" alt="item photo"></a>
            </div> <!-- end item -->
            <div class="item">
              <a href="images/app2.jpg" data-rel="prettyPhoto"><img src="images/app2.jpg" alt="item photo"></a>
            </div> <!-- end item -->
            <div class="item">
              <a href="images/app.jpg" data-rel="prettyPhoto"><img src="images/app.jpg" alt="item photo"></a>
            </div> <!-- end item -->
            <div class="item">
              <a href="images/app2.jpg" data-rel="prettyPhoto"><img src="images/app2.jpg" alt="item photo"></a>
            </div> <!-- end item -->
          </div> <!-- end owl carousel -->

        </div> <!-- .container -->
      </div> <!-- end .screenshots -->  
    </section>
        --%>
        <!-- ====== End Screenshots Section ====== -->
        <!-- ====== Description Section ====== -->

        <!-- ====== End Description Section ====== -->
        <!-- ====== Testimonial Section ====== -->
        <%--     <section id="testimonial">
      <div class="bg-color bg-testimonial">
        <div class="testimonial section-padding">
          <div class="container">
            <div class="testimonial-slide">
              <div id="carousel-testimonial" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-testimonial" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-testimonial" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-testimonial" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">

                  <div class="item">
                    <div class="image">
                      <img src="images/01.jpg" alt="">
                    </div> <!-- end .image -->
                    <div class="content">
                      <p>Want more Bootstrap themes & templates? Subscribe to our mailing list to receive an update when new items arrive! Akshara is a library to buy Bootstrap themes and templates for your business need. Want more Bootstrap themes & templates ? Want more Bootstrap themes & templates?</p>
                      <h4>Jon Doe</h4>
                      <h5>Web developer</h5>
                    </div> <!-- end .content -->
                  </div> <!-- end .item (1) -->

                  <div class="item active left">
                    <div class="image">
                      <img src="images/02.jpg" alt="">
                    </div> <!-- end .image -->
                    <div class="content">
                      <p>Want more Bootstrap themes & templates? Subscribe to our mailing list to receive an update when new items arrive! Akshara is a library to buy Bootstrap themes and templates for your business need. Want more Bootstrap themes & templates ? Want more Bootstrap themes & templates?</p>
                      <h4>Jon Doe</h4>
                      <h5>Web developer</h5>
                    </div> <!-- end .content -->
                  </div> <!-- end .item (2) -->

                  <div class="item next left">
                    <div class="image">
                      <img src="images/03.jpg" alt="">
                    </div> <!-- end .image -->
                    <div class="content">
                      <p>Want more Bootstrap themes & templates? Subscribe to our mailing list to receive an update when new items arrive! Akshara is a library to buy Bootstrap themes and templates for your business need. Want more Bootstrap themes & templates ? Want more Bootstrap themes & templates?</p>
                      <h4>Jon Doe</h4>
                      <h5>Web developer</h5>
                    </div> <!-- end .content -->
                  </div> <!-- end .item (3) -->

                </div> <!-- end .carousel-inner -->
              </div> <!-- end #carousel-testimonial -->
            </div> <!-- end .testimonial-slide -->
          </div> <!-- end .container -->
        </div> <!-- end .testimonial -->
      </div> <!-- end .bg-testimonial -->
    </section>
        --%>
        <!-- ====== End Testimonial Section ====== -->
        <!-- ====== Team Section ====== -->
        <%--    <section id="team">
      <div class="team section-padding">
        <div class="container">

          <div class="header">
            <h1><strong>Who develop the App</strong></h1>
            <p>Want more Bootstrap themes & templates? Subscribe to our mailing list to receive an update when new items arrive! Akshara is a library to buy Bootstrap themes and templates for your business need. Want more Bootstrap themes & templates ? Want more Bootstrap themes & templates?</p>
            <div class="underline"><i class="fa fa-users"></i></div>
          </div> <!-- end .container> .header -->

          <div class="row">
            <div class="app-dev">

              <div class="col-sm-6 col-md-6 col-lg-3 info">
                <div class="member">
                  <img src="images/01.jpg" alt="">
                  <div class="details">
                    <p>Want more Bootstrap themes & templates? Subscribe to our mailing list to receive an update when new items arrive! </p>
                    <div class="social icon">
                      <ul>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                      </ul>
                    </div> <!-- end .icon -->
                  </div> <!-- end .details -->
                </div> <!-- end .member -->
                <div class="title">
                  <h4>Margery Key</h4>
                  <h5>Lead Developer</h5>
                </div> <!-- end .title -->
              </div> <!-- end .info (1) -->

              <div class="col-sm-6 col-md-6 col-lg-3 info">
                <div class="member">
                  <img src="images/02.jpg" alt="">
                  <div class="details">
                    <p>Want more Bootstrap themes & templates? Subscribe to our mailing list to receive an update when new items arrive! </p>
                    <div class="social icon">
                      <ul>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                      </ul>
                    </div> <!-- end .icon -->
                  </div> <!-- end .details -->
                </div> <!-- end .member -->
                <div class="title">
                  <h4>Shirley Bergeron</h4>
                  <h5>UI/UX Designer</h5>
                </div> <!-- end .title -->
              </div> <!-- end .info (2) -->

              <div class="col-sm-6 col-md-6 col-lg-3 info">
                <div class="member">
                  <img src="images/03.jpg" alt="">
                  <div class="details">
                    <p>Want more Bootstrap themes & templates? Subscribe to our mailing list to receive an update when new items arrive! </p>
                    <div class="social icon">
                      <ul>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                      </ul>
                    </div> <!-- end .icon -->
                  </div> <!-- end .details -->
                </div> <!-- end .member -->
                <div class="title">
                  <h4>Ryan Elder</h4>
                  <h5>Front End Developer</h5>
                </div> <!-- end .title -->
              </div> <!-- end .info (3) -->

              <div class="col-sm-6 col-md-6 col-lg-3 info">
                <div class="member">
                  <img src="images/04.jpg" alt="">
                  <div class="details">
                    <p>Want more Bootstrap themes & templates? Subscribe to our mailing list to receive an update when new items arrive! </p>
                    <div class="social icon">
                      <ul>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                      </ul>
                    </div> <!-- end .icon -->
                  </div> <!-- end .details -->
                </div> <!-- end .member -->
                <div class="title">
                  <h4>Daisy Baker</h4>
                  <h5>Marketer</h5>
                </div> <!-- end .title -->
              </div> <!-- end .info (4) -->

            </div> <!-- end .app-dev -->
          </div> <!-- end .container> .row -->

        </div> <!-- end .container -->
      </div> <!-- end .team -->
    </section>
        --%>
        <!-- ====== Team Section ====== -->
        <!-- ====== Price Section ====== -->
      
        <!-- ====== End Price Section ====== -->
        <!-- ====== Subscribe Section ====== -->
      <%--  <section id="subscribe">
      <div class="subscribe section-padding">
        <div class="container">
          <div class="subscribe-header">
            <h1>Newsletter</h1>
            <form action="" class="form subscribe-form">
              <div class="form-group">
                <div class="input-group">
                  <label for="f-name" class="sr-only">Newsletter</label>
                  <input type="text" class="form-control" id="f-name" placeholder="Enter your email address">
                
                  <div class="input-group-addon">
                    <button class="btn" type="submit">SUBMIT</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="social">
            <ul>
              <li><a class="twitter" href=""><i class="fa fa-twitter"></i></a></li> 
              <li><a class="facebook" href=""><i class="fa fa-facebook"></i></a></li> 
              <li><a class="google-plus" href=""><i class="fa fa-google-plus"></i></a></li> 
              <li><a class="youtube" href=""><i class="fa fa-youtube"></i></a></li> 
              <li><a class="linkedin" href=""><i class="fa fa-linkedin"></i></a></li> 
              <li><a class="pinterest" href=""><i class="fa fa-pinterest"></i></a></li> 
              <li><a class="dribbble" href=""><i class="fa fa-dribbble"></i></a></li> 
              <li><a class="flickr" href=""><i class="fa fa-flickr"></i></a></li>          
            </ul>
          </div>
        </div>
      </div>
    </section>--%>
        <!-- ====== End Subscribe Section ====== -->
        <!-- ====== Download Section ====== -->
      
        <!-- ====== End Download Section ====== -->
        <!-- ====== Contact Section ====== -->
        <footer id="contact">
      <div class="footer section-padding">
        <!-- end .container -->


          <div class="container">
              <p>
              1.	General terms 
2.1  Intellectual Property Rights
The Service Provider agrees to grant to the Buyer a non-exclusive, irrevocable, royalty free licence to use, copy and modify any elements of the Material not specifically created for the Buyer as part of the Services. In respect of the Material specifically created for the Buyer as part of the Services, the Service Provider assigns the full title guarantee to the Buyer and any all of the copyright, other intellectual property rights and any other data or material used or subsisting in the Material whether finished or unfinished. If any third party intellectual property rights are used in the Material the Service Provider shall ensure that it has secured all necessary consents and approvals to use such third party intellectual property rights for the Service Provider and the Buyer. For the purposes of this Clause 2.1, "Material" shall mean the materials, in whatever form, used by the Service Provider to provide the Services and the products, systems, programs or processes, in whatever form, produced by the Service Provider pursuant to this Agreement.
2.2  Warranty 
a.	The Service Provider represents and warrants that: 
i.	it will perform the Services with reasonable care and skill; and 
ii.	the Services and the Materials provided by the Service Provider to the Buyer under this Agreement will not infringe or violate any intellectual property rights or other right of any third party. 
2.3  Limitation of liability 
b.	Subject to the Buyer’s obligation to pay the Price to the Service Provider, either party’s liability in contract, tort or otherwise (including negligence) arising directly out of or in connection with this Agreement or the performance or observance of its obligations under this Agreement and every applicable part of it shall be limited in aggregate to the Price. 
c.	To the extent it is lawful to exclude the following heads of loss and subject to the Buyer’s obligation to pay the Price, in no event shall either party be liable for any loss of profits, goodwill, loss of business, loss of data or any other indirect or consequential loss or damage whatsoever. 
d.	Nothing in this Clause 2.3 will serve to limit or exclude either Party’s liability for death or personal injury arising from its own negligence. 
2.4  Term and Termination 
e.	This Agreement shall be effective on the date hereof and shall continue, unless terminated sooner in accordance with Clause 2.4(b), until the Completion Date. 
f.	Either Party may terminate this Agreement upon notice in writing if: 
i.	the other is in breach of any material obligation contained in this Agreement, which is not remedied (if the same is capable of being remedied) within 30 days of written notice from the other Party so to do; or 
ii.	a voluntary arrangement is approved, a bankruptcy or an administration order is made or a receiver or administrative receiver is appointed over any of the other Party's assets or an undertaking or a resolution or petition to wind up the other Party is passed or presented (other than for the purposes of amalgamation or reconstruction) or any analogous procedure in the country of incorporation of either party or if any circumstances arise which entitle the Court or a creditor to appoint a receiver, administrative receiver or administrator or to present a winding-up petition or make a winding-up order in respect of the other Party. 
g.	[For European Buyers and Service Providers only] If the Buyer is a consumer and the Distance Selling Directive (97/7/EC) (the "Directive") applies to this Agreement, the Buyer may terminate this Agreement within the relevant timescales prescribed by the regulations or laws in the relevant Member State which implement the requirements of the Directive in respect of a right for the Buyer to withdraw from a contract. In the event of termination in accordance with this Clause 2.4(c), the liability of the Buyer to the Service Provider shall be as prescribed in the Directive or in any regulations or laws implementing its requirements in the relevant Member States. 
h.	Any termination of this Agreement (howsoever occasioned) shall not affect any accrued rights or liabilities of either Party nor shall it affect the coming into force or the continuance in force of any provision hereof which is expressly or by implication intended to come into or continue in force on or after such termination. 
                  </p>

          </div>

      </div> <!-- end .footer -->
    </footer>
        <!-- ====== End Contact Section ====== -->
        <!-- ====== Copyright Section ====== -->
        <section class="copyright dark-bg">
      <div class="container">
      <div class="row">
      <div class="col-lg-5 col-md-5">
        <p>Copyright &copy; 2015, Elite Crest Technologies, All Rights Reserved</p>
        </div>
         <div class="col-lg-5 col-md-5">
        <p><strong></strong></p>
        </div>
        </div>
      </div> <!-- end .container -->
    </section>
        <!-- ====== End Copyright Section ====== -->
        <!-- jQuery -->
        <script src="http://code.jquery.com/jquery.min.js"></script>
        <script src="Scripts/jquery.js"></script>
        <!-- Modernizr js -->
        <script src="Scripts/modernizr-latest.js"></script>
        <!-- Bootstrap 3.2.0 js -->
        <script src="Scripts/bootstrap.min.js"></script>
        <!-- Owl Carousel plugin -->
        <script src="Scripts/owl.carousel.min.js"></script>
        <!-- ScrollTo js -->
        <script src="Scripts/jquery.scrollto.min.js"></script>
        <!-- LocalScroll js -->
        <script src="Scripts/jquery.localScroll.min.js"></script>
        <!-- jQuery Parallax plugin -->
        <script src="Scripts/jquery.parallax-1.1.3.js"></script>
        <!-- Skrollr js plugin -->
        <script src="Scripts/skrollr.min.js"></script>
        <!-- jQuery One Page Nav Plugin -->
        <script src="Scripts/jquery.nav.js"></script>
        <!-- jQuery Pretty Photo Plugin -->
        <script src="Scripts/jquery.prettyPhoto.js"></script>
        <!-- Custom JS -->
        <script src="Scripts/main.js"></script>
        <script>
            function OpenInGoogleTab() {
                var win = window.open("http://goo.gl/YbvJvN", '_blank');
                win.focus();
            } 
            function OpenInIphoneTab() {
                var win = window.open("http://goo.gl/mHBuHa", '_blank');
                win.focus();
            }
            jQuery(document).ready(function ($) {
                "use strict";

                jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({ social_tools: false });
            });
        </script>
    </form>
</body>
</html>

