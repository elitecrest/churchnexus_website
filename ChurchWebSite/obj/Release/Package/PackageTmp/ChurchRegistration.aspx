﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChurchRegistration.aspx.cs" Inherits="ChurchWebSite.ChurchRegistration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Template Title -->
    <title>Church Nexus</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- Bootstrap 3.2.0 stylesheet -->
    <link href="Content/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome Icon stylesheet -->
    <link href="Content/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Owl Carousel stylesheet -->
    <link href="Content/owl.carousel.css" rel="stylesheet">
    <!-- Pretty Photo stylesheet -->
    <link href="Content/prettyPhoto.css" rel="stylesheet">
    <!-- Custom stylesheet -->
    <link href="Content/style.css" rel="stylesheet" type="text/css" />
    <link href="Content/white.css" rel="stylesheet" type="text/css" />

    <!-- Custom Responsive stylesheet -->
    <link href="Content/responsive.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="Scripts/jquery.min.js"></script>
    <script src="Scripts/jquery-1.10.2.js"></script>
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Scripts/jquery.js"></script>
      <!-- ============ Add custom CSS here ============ -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="Content/styles.css" rel="stylesheet" type="text/css" />
    <link href="Content/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="Content/template.css" rel="stylesheet" type="text/css" />
<link href="Content/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
<script src="Scripts/jquery-1.6.min.js" type="text/javascript"></script>
<script src="Scripts/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="Scripts/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<%--<script type="text/javascript">--%>
    <script src="Scripts/UserValidation.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <!-- ====== Header Section ====== -->
       
        <!-- ====== End Header Section ====== -->
        <!-- ====== Menu Section ====== -->
        <section id="menu">
      <div class="navigation">
        <div id="main-nav" class="navbar navbar-default" role="navigation">
          <div class="container">

            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                 <div class="navbar-brand" > <img src="images/churchnexus1.png"/ alt=" " /></div>
              <%--<a class="navbar-brand" href="images/churchnexuslogo.png">Church Nexus</a>--%>
            </div> <!-- end .navbar-header -->

            <div class="navbar-collapse collapse">
             
            </div><!-- end .navbar-collapse -->

          </div> <!-- end .container -->
        </div> <!-- end #main-nav -->
      </div> <!-- end .navigation -->
    </section>
       
        <footer id="contact">
      <div class="footer section-padding">
        <!-- end .container -->


          <div class="container">
            <div class="form-group">
                                <asp:Label ID="results" runat="server" ForeColor="Green" Visible="false" CssClass="col-md-4 control-label"></asp:Label>
                            </div>
        <div class="container">
            <%--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
            <%--<div id="banner">
                <h1>
                    Church <strong>Registration Form</strong></h1>
            </div>
        </div>--%>
            <div class="col-lg-6 col-lg-6 col-mf-6 col-md-12">
                <div class="registrationform">
                    <div class="form-horizontal">
                        <fieldset>
                            <div class="form-group">
                                <asp:Label ID="Label5" runat="server" Text="Country" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <asp:DropDownList ID="churchcountry" runat="server" CssClass="form-control ddl">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">India</asp:ListItem>
                                        <asp:ListItem Value="2">United States</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label7" runat="server" Text="Church Name" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <%--<label class="error"></label>--%>
                                    <asp:TextBox ID="ChurchName" runat="server" placeholder="Church Name" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-sm-2">
                                    <asp:Label ID="churcherror" runat="server" Visible="false"></asp:Label>
                                </div>
                            </div>

                            <div class="form-group">
                                <asp:Label ID="Label4" runat="server" Text="Admin First Name" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <label class="error"></label>
                                    <asp:TextBox ID="AdminFirstName" runat="server" placeholder="Admin First Name" CssClass="form-control"></asp:TextBox><%--><asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" ControlToValidate="AdminFirstName" runat="server" ErrorMessage="*First Name is required"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label6" runat="server" Text="Admin Last Name" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <label class="error"></label>
                                    <asp:TextBox ID="AdminLastName" runat="server" placeholder="Admin Last Name" CssClass="form-control"></asp:TextBox><%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ForeColor="Red" ControlToValidate="AdminLastName" ErrorMessage="*Last Name is required"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label1" runat="server" Text="Email" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <label class="error"></label>
                                    <asp:TextBox ID="Email" runat="server" placeholder="Email" CssClass="form-control"></asp:TextBox><%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ForeColor="Red" ControlToValidate="Email" ErrorMessage="*Mandatory Field"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Password" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="Password" runat="server" placeholder="Password" CssClass="form-control"
                                        TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="lblConfirmPassword" runat="server" ForeColor="Black" Text="Confirm Password" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="ConfirmPassword" runat="server" placeholder="Confirm Password" CssClass="form-control"
                                        TextMode="Password"></asp:TextBox>
                                </div>
                               <%-- <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="ConfirmPassword" ControlToValidate="Password" ForeColor="Red" ErrorMessage="Password is not matching"></asp:CompareValidator>--%>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label8" runat="server" Text="Give URL" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="GiveURL" runat="server" placeholder="Give URL" CssClass="form-control"></asp:TextBox>
                                </div>
                               <%-- <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="ConfirmPassword" ControlToValidate="Password" ForeColor="Red" ErrorMessage="Password is not matching"></asp:CompareValidator>--%>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label12" runat="server" Text="Contact Number" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="ContactNumber" runat="server" TextMode="Number" placeholder="Contact Number" MaxLength="10" CssClass="form-control"></asp:TextBox><%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidateRequestMode="Enabled" ValidationExpression="[0-9]{10}" runat="server" ControlToValidate="ContactNumber" ForeColor="Red" ErrorMessage="Contact number should have maximum of 10 digits"></asp:RegularExpressionValidator>--%>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label3" runat="server" Text="Address" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="ChurchAddress" runat="server" TextMode="MultiLine" Rows="1" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label9" runat="server" Text="City" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="ChurchCity" runat="server" placeholder="City" CssClass="form-control"></asp:TextBox><%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ChurchCity" runat="server" ForeColor="Red" ErrorMessage="City is mandatory"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label10" runat="server" Text="State" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="ChurchState" runat="server" placeholder="State" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label11" runat="server" Text="Zip Code" ForeColor="Black" CssClass="col-md-4 control-label"></asp:Label>
                                <div class="col-md-6">
                                    <asp:TextBox ID="ZipCode" runat="server" placeholder="Zip Code" CssClass="form-control"></asp:TextBox><%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ForeColor="Red" ControlToValidate="ZipCode" ErrorMessage="Please enter proper Zip Code"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <br />
                                    <asp:HiddenField ID="btnCancel" runat="server"/>
                                    <asp:Button ID="btnSubmit" runat="server" ForeColor="Black" CssClass="btn btn-primary" OnClientClick="validation()" Text="Register" OnClick="btnSubmit_Click" />
                                    
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>

          </div>

      </div> <!-- end .footer -->
    </footer>
        <!-- ====== End Contact Section ====== -->
        <!-- ====== Copyright Section ====== -->
        <section class="copyright dark-bg">
      <div class="container">
      <div class="row">
      <div class="col-lg-5 col-md-5">
        <p>Copyright &copy; 2015, Elite Crest Technologies, All Rights Reserved</p>
        </div>
         <div class="col-lg-5 col-md-5">
        <p><strong></strong></p>
        </div>
        </div>
      </div> <!-- end .container -->
    </section>
        <!-- ====== End Copyright Section ====== -->
        <!-- jQuery -->
        <script src="http://code.jquery.com/jquery.min.js"></script>
        <script src="Scripts/jquery.js"></script>
        <!-- Modernizr js -->
        <script src="Scripts/modernizr-latest.js"></script>
        <!-- Bootstrap 3.2.0 js -->
        <script src="Scripts/bootstrap.min.js"></script>
        <!-- Owl Carousel plugin -->
        <script src="Scripts/owl.carousel.min.js"></script>
        <!-- ScrollTo js -->
        <script src="Scripts/jquery.scrollto.min.js"></script>
        <!-- LocalScroll js -->
        <script src="Scripts/jquery.localScroll.min.js"></script>
        <!-- jQuery Parallax plugin -->
        <script src="Scripts/jquery.parallax-1.1.3.js"></script>
        <!-- Skrollr js plugin -->
        <script src="Scripts/skrollr.min.js"></script>
        <!-- jQuery One Page Nav Plugin -->
        <script src="Scripts/jquery.nav.js"></script>
        <!-- jQuery Pretty Photo Plugin -->
        <script src="Scripts/jquery.prettyPhoto.js"></script>
        <!-- Custom JS -->
        <script src="Scripts/main.js"></script>
        <script>
            function OpenInGoogleTab() {
                var win = window.open("http://goo.gl/YbvJvN", '_blank');
                win.focus();
            } 
            function OpenInIphoneTab() {
                var win = window.open("http://goo.gl/mHBuHa", '_blank');
                win.focus();
            }
            jQuery(document).ready(function ($) {
                "use strict";

                jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({ social_tools: false });
            });
        </script>
    </form>
</body>
</html>

