﻿$(document).ready(function () {
    $('#btnSubmit').click(function () {
        var churchName, pass, password, confirmPassword, giveURL, contactNumber, churchAddress, churchCity,
            zipCode, churchState, gender, conpass, EmailId, emailExp, zipcode, state, FirstName, LastName,
           // var churchName
           debugger;

            churchName = $("#ChurchName").val();
        FirstName = $("#AdminFirstName").val();
        LastName = $("#AdminLastName").val();
        password = $("#Password").val();
        confirmPassword = $("#ConfirmPassword").val();
        giveURL = $("#GiveURL").val();
        contactNumber = $("#ContactNumber").val();
        churchAddress = $("#ChurchAddress").val();
        churchCity = $("#ChurchCity").val();
        churchState = $("#ChurchState").val();
        zipCode=$("#ZipCode").val();
        gender = $("#ddlType").val();
        pass = $("#txtPass1").val();
        conpass = $("#txtPass2").val();
        EmailId = $("#txtmail").val();
        zipcode = $("#ZipCode").val();
        state = $("#ChurchState").val();
        country = $("#churchcountry").val();

        emailExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([com\co\.\in])+$/;


        //$("#btnSubmit").validate({
        //    rules: {
        //        ChurchName: { required: true }, AdminFirstName: { required: true }, AdminLastName: { required: true },
        //        //Author: { required: true },
        //        ////Description: { required: true },
        //        //Updated: { required: true }, SermonType: { required: true }
        //    },
        //    messages: {
        //        ChurchName: { required: "Church Name is required" },
        //        AdminFirstName: { required: "Admin first Name required" },
        //        AdminlastName: { required: "Admin last Name required" },
        //        //Author: { required: "Speaker required" },
        //        ////Description: { required: "Description required" },
        //        //Updated: { required: "Happened Date required" },
        //        //SermonType: { required: "Sermon Type required" }
        //    },
        //});

        if (churchName.length == 0 && FirstName.length == 0 && LastName.length == 0 && password.length == 0 &&
            confirmPassword.length == 0 && giveURL.length == 0 && contactNumber.length == 0 && churchAddress.length == 0 &&
            churchCity.length == 0 && churchState.length == 0) {

            alert("Please don't leave the fields empty");
            return false;
        }
        if (churchName.length == 0) {
            //$('#ChurchName').parent('.form-group').append("<label class='error'>Church Name is required</label>");
            $("#churcherror").removeClass(hide);
            $("#churcherror").text = "Church Name is required";
            return false;
        }
        if (gender == 0) {

            alert("Please Select gender");

            return false;
        }
        if (pass == '') {
            alert("Please Enter Password");

            return false;
        }
        if (pass != '' && conpass == '') {
            alert("Please Confirm Password");

            return false;

        }
        if (pass != conpass) {
            alert("Password not match");

            return false;

        }
        if (EmailId == '') {
            alert("Email Id Is Required");

            return false;
        }
        if (EmailId != '') {


            if (!EmailId.match(emailExp)) {
                alert("Invalid Email Id");

                return false;
            }
        }
        return true;
    })
});